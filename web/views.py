import json

from django.http import HttpResponse, Http404
from django.shortcuts import render, get_object_or_404

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User

from .models import Notification, Wallet, Token


def _map_notifications(obj):
    obj.update(json.loads(obj['data']))
    del obj['data']
    return obj


def login_required_token(function):
    def decorator(request, *args, **kwargs):
        token = request.REQUEST.get('token', None)
        user = Token.verify(token)
        if not user:
            raise Http404
        request.user = user
        return function(request, *args, **kwargs)
    return decorator

@login_required_token
def user_notification_all(request):
    data = map(_map_notifications,
               Notification.objects.filter(state=Notification.NEW, user_id=request.user.id).values('id', 'data'))
    return HttpResponse(json.dumps({'data': data}))


@login_required_token
def user_notification_object(request, notification_id):
    # TODO: only DELETE method
    obj = get_object_or_404(Notification, state=Notification.NEW, user_id=request.user.id, id=notification_id)
    obj.state = Notification.SENT
    obj.save()
    return HttpResponse('ok')


@login_required_token
def user_wallet_object(request, wallet_id):
    # TODO: only DELETE method
    obj = get_object_or_404(Wallet, user_id=request.user.id, id=wallet_id)
    obj.delete()
    return HttpResponse('ok')


@login_required_token
def user_wallet(request):
    if request.method == 'POST':
        request_body_json = json.loads(request.body)
        wallet = request_body_json['wallet']
        wallet_name = request_body_json['wallet_name']
        Wallet.new(request.user.id, wallet=wallet, wallet_name=wallet_name)
        return HttpResponse('ok')
    data = list(Wallet.objects.filter(user_id=request.user.id).values('id', 'wallet', 'wallet_name'))
    return HttpResponse(json.dumps({'data': data, }))


def login_token(request):
    request_body_json = json.loads(request.body)
    username = request_body_json['username']
    password = request_body_json['password']
    user = authenticate(username=username, password=password)
    if not user:
        if User.objects.filter(username=username).exists():
            raise Http404
        user = User(username=username)
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
    login(request, user)
    token = Token.generate(user.id)
    return HttpResponse(token)


def home(request):
    return render(request, 'home.html')