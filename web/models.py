import time
import json
import random
import string

import requests

from django.db import models

from django.contrib.auth.models import User


WALLET_INFO_URL = 'https://blockchain.info/address/{wallet}?format=json'


def get_wallet_info(wallet):
    return requests.get(WALLET_INFO_URL.format(wallet=wallet)).json()


def get_random_string(length):
    return ''.join([random.choice(string.ascii_letters+string.digits) for i in xrange(length)])


class Notification(models.Model):
    NEW = 1
    SENT = 2
    STATES = {
        NEW: 'new',
        SENT: 'sent',
    }

    user = models.ForeignKey(User)
    state = models.IntegerField(choices=STATES.items(), default=NEW)
    data = models.TextField()

    def __unicode__(self):
        return u'%s:(%s) %s' % (self.user, self.STATES[self.state], self.data)

    @staticmethod
    def new(user_id, data):
        obj = Notification(
            user_id=user_id,
            data=data,
        )
        obj.save()
        return obj


class Wallet(models.Model):
    user = models.ForeignKey(User)
    wallet = models.CharField(max_length=54)
    wallet_name = models.CharField(max_length=32, default='', blank=True)

    last_n_tx = models.IntegerField()
    last_bal = models.IntegerField()

    def __unicode__(self):
        return u'%s:%s (%s)' % (self.user.username, self.wallet, self.wallet_name)

    class Meta:
        unique_together = (('user', 'wallet'), )

    def check(self):
        wallet_info = get_wallet_info(self.wallet)
        if wallet_info['n_tx'] > self.last_n_tx:
            amount = wallet_info['final_balance'] - self.last_bal
            timestamp = int(time.time())
            data = json.dumps({
                'amount': amount,
                'timestamp': timestamp,
                'wallet': self.wallet,
                'wallet_name': self.wallet_name,
            })
            Notification.new(user_id=self.user_id, data=data)
            self.last_bal = wallet_info['final_balance']
            self.last_n_tx = wallet_info['n_tx']
            self.save()

    @staticmethod
    def new(user_id, wallet, wallet_name=''):
        wallet_info = get_wallet_info(wallet)
        obj = Wallet(
            user_id=user_id,
            wallet=wallet,
            wallet_name=wallet_name,
            last_n_tx=wallet_info['n_tx'],
            last_bal=wallet_info['final_balance'],
        )
        obj.save()
        return obj


class Token(models.Model):
    user = models.OneToOneField(User)
    token = models.CharField(max_length=64)

    def __unicode__(self):
        return u'%s:%s' % (self.user.username, self.token)

    @staticmethod
    def generate(user_id):
        obj, created = Token.objects.get_or_create(user_id=user_id)
        obj.token = get_random_string(30)
        obj.save()
        return obj.token

    @staticmethod
    def verify(token):
        try:
            return Token.objects.get(token=token).user
        except Token.DoesNotExist:
            return None