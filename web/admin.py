from django.contrib import admin

from .models import Wallet, Notification, Token


admin.site.register(Wallet)
admin.site.register(Notification)
admin.site.register(Token)
