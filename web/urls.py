from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'web.views.home'),
    url(r'^api/v1/login/$', 'web.views.login_token'),
    url(r'^api/v1/wallet/$', 'web.views.user_wallet'),
    url(r'^api/v1/wallet/(?P<wallet_id>\d+)/$', 'web.views.user_wallet_object'),
    url(r'^api/v1/notification/$', 'web.views.user_notification_all'),
    url(r'^api/v1/notification/(?P<notification_id>\d+)/$', 'web.views.user_notification_object'),
)
