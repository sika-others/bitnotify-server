# [BitNotify](http://bn.pya.cz)

Bitcoin payments notifications to smart watch.

![BitNotify Logo](bitnotify.png)

## API (v1)

### `POST /api/v1/login/`

Return token. If username does not exist, user will be register.

```
> {"username": "sika", "password": "asdf"}
< 1BZGxE
```

### `POST /api/v1/wallet/?token=1BZGxE`

Add new wallet

```
> {"wallet": "1JN3LoHYkdThYFpLpcyVTw3GQDkMGGhMuD", "wallet_name": "personal"}
< ok
```

### `GET /api/v1/wallet/?token=1BZGxE`

Return list of wallet

```
< {"data": [{"wallet": "17YzKaX91ACABC2Duaon9WFc85FFBaHARc", "wallet_name": "", "id": 1}]}
```

### `GET /api/v1/notification/?token=1BZGxE`

Return list of new notifications

```
< {"data": [{"timestamp": 1407586824, "amount": 18981000, "wallet_name": "", "wallet": "17YzKaX91ACABC2Duaon9WFc85FFBaHARc", "id": 1}]}
```

### `DELETE /api/v1/notification/{notification_id}/?token=1BZGxE`

Mark notification as send

```
< ok
```

## Example API requests via curl

``` bash
# login
curl -H "Content-Type: application/json" -d '{"username":"sika", "password":"asdf"}' http://bn.pya.cz/api/v1/login/

# add wallet
curl -H "Content-Type: application/json" -d '{"wallet": "1JN3LoHYkdThYFpLpcyVTw3GQDkMGGhMuD", "wallet_name": "personal"}' http://bn.pya.cz/api/v1/wallet/?token=1BZGxE 

# get wallets
curl http://bn.pya.cz/api/v1/wallet/?token=1BZGxE

# get notifications
curl http://bn.pya.cz/api/v1/notification/?token=1BZGxE

```
